git clone https://chromium.googlesource.com/chromium/tools/depot_tools.git

export PATH=$PATH:$PWD/depot_tools
export DEPOT_TOOLS_UPDATE=0

grep "android stable 103.0.5060.129" from:

https://omahaproxy.appspot.com/

or just last build from:

https://pdfium.googlesource.com/pdfium/+refs

grab last commit from:

https://pdfium.googlesource.com/pdfium/+/chromium/5060

setup repo:

mkdir repo
cd repo
gclient config --unmanaged --custom-var=checkout_android=True --name pdfium https://pdfium.googlesource.com/pdfium.git@57e40e8

sync:

gclient sync

cd pdfium

patch -p1 < ../../build.diff
./build/install-build-deps-android.sh
../../build.sh

# Links

* https://medium.com/@raju.kandasamy/how-to-build-pdfium-library-for-android-70e42ad31f6c